package persistence;

import user.User;

import java.util.List;

public interface UserPersistenceService {
    void addUser(User user) throws Exception;

    void updateUser(User user) throws Exception;

    void deleteUser(String username) throws Exception;

    User findUser(String username) throws Exception;

    List<User> getUsers() throws Exception;
}
