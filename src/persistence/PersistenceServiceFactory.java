package persistence;

public interface PersistenceServiceFactory {
    UserPersistenceService createUserPersistenceService();
}
