package persistence;

import user.User;

import java.util.List;

public class LocalUserPersistenceService implements UserPersistenceService {
    LocalUserPersistenceService() {

    }

    @Override
    public void addUser(User user) throws Exception {

    }

    @Override
    public void updateUser(User user) throws Exception {

    }

    @Override
    public void deleteUser(String username) throws Exception {

    }

    @Override
    public User findUser(String username) throws Exception {
        return null;
    }

    @Override
    public List<User> getUsers() throws Exception {
        return null;
    }
}
