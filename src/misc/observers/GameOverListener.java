package misc.observers;

import game.Player;

import java.util.Map;

public interface GameOverListener {
    void doAction(Player winner, Map<Player, Integer> scoreInc);
}
