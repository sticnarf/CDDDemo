package misc.observers;

import game.Card;

public interface CardInterationListener {
    void doAction(Card[] card);
}
