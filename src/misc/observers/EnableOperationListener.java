package misc.observers;

import game.Player;

public interface EnableOperationListener {
    void doAction(Player player);
}
