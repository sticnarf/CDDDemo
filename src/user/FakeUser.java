package user;

import java.awt.*;

public class FakeUser extends User {
    public FakeUser(String nickname, Image avatar) {
        super(nickname, avatar);
    }
}
