package user;

import java.awt.*;

public class LocalUser extends User {
    private String username;

    LocalUser(String username, String nickname, Image avatar) {
        super(nickname, avatar);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAvatar(Image avatar) {
        this.avatar = avatar;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
