package user;

import java.awt.*;

public abstract class User {
    String nickname;
    Image avatar;
    int points;

    User(String nickname, Image avatar) {
        this.nickname = nickname;
        this.avatar = avatar;
        this.points = 0;
    }

    public String getNickname() {
        return nickname;
    }

    public Image getAvatar() {
        return avatar;
    }

    public int getPoints() {
        return points;
    }

    public void updatePoints(int inc) {
        points += inc;
    }
}
