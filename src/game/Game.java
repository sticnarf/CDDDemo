package game;

import game.rules.CDDRule;
import misc.observers.CardInterationListener;
import misc.observers.EnableOperationListener;
import misc.observers.GameOverListener;

import java.util.List;
import java.util.Map;

public class Game {
    private CDDRule rule;
    private Player[] players;
    private Player currentOperator;
    private Card[] lastCombination;
    private List<EnableOperationListener> enableOperationListeners;
    private List<CardInterationListener> discardCardsListeners;
    private List<GameOverListener> gameOverListeners;

    private Game() {
        players = new Player[4];
        // TODO Initial listeners lists
    }

    public class Factory {
        int playerCount = 0;

        public Game create() {
            return Game.this;
        }

        public void setRule(CDDRule rule) {
            Game.this.rule = rule;
        }

        public void addPlayer(Player players) {
            Game.this.players[playerCount++] = players;
        }
    }

    public void start() {
        Card[] deck = rule.generateDeck();
        rule.deal(deck, players);
        currentOperator = rule.decideOperator(currentOperator, players);
        // inform all
        for (EnableOperationListener listener : enableOperationListeners) {
            listener.doAction(currentOperator);
        }
    }

    public void end(Player winner) {
        Map<Player, Integer> scores = rule.calculateScore(winner, players);
        for (GameOverListener listener : gameOverListeners) {
            listener.doAction(winner, scores);
        }
    }

    public void registerEnableOperationListener(EnableOperationListener listener) {
        enableOperationListeners.add(listener);
    }

    public void registerDiscardCardsListener(CardInterationListener listener) {
        discardCardsListeners.add(listener);
    }

    public void setGameOverListener(GameOverListener gameOverListener) {
        this.gameOverListeners.add(gameOverListener);
    }

    void playerDiscard(Player player, Card[] combination) {
        if (rule.validateCombination(combination, lastCombination)) {
            // remove cards from hand
            player.removeCards(combination);

            // inform UI to show cards or others
            for (CardInterationListener listener : discardCardsListeners) {
                listener.doAction(combination);
            }

            // if hand is empty
            if (player.hand.isEmpty())
                end(player);
        }
    }
}
