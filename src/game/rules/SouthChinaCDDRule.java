package game.rules;

import game.Card;
import game.Player;

import java.util.Map;

public class SouthChinaCDDRule implements CDDRule {
    @Override
    public Card[] generateDeck() {
        return new Card[0];
    }

    @Override
    public void deal(Card[] deck, Player[] players) {

    }

    @Override
    public Player decideOperator(Player lastOperator, Player[] players) {
        return null;
    }

    @Override
    public boolean validateCombination(Card[] comb, Card[] last) {
        return false;
    }

    @Override
    public Map<Player, Integer> calculateScore(Player winner, Player[] players) {
        return null;
    }
}
