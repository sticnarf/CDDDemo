package game.rules;

import game.Card;
import game.Player;

import java.util.Map;

public interface CDDRule {
    Card[] generateDeck();

    void deal(Card[] deck, Player[] players);

    Player decideOperator(Player lastOperator, Player[] players);

    boolean validateCombination(Card[] comb, Card[] last);

    Map<Player, Integer> calculateScore(Player winner, Player[] players);
}
