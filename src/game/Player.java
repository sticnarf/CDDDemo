package game;

import misc.observers.CardInterationListener;
import misc.observers.EnableOperationListener;
import user.User;

import java.util.List;

public abstract class Player {
    Game game;
    User user;
    List<Card> hand;
    private List<CardInterationListener> addCardsListeners;
    private List<CardInterationListener> removeCardsListeners;

    Player(Game game, User user) {
        this.game = game;
        this.user = user;
        game.registerEnableOperationListener(new DefaultEnableOperationListener());
    }

    public void addCards(Card[] cards) {
    }

    public void removeCards(Card[] cards) {

    }

    public void clearCards() {

    }

    public void registerAddCardListener(CardInterationListener listener) {
        addCardsListeners.add(listener);
    }

    public void registerRemoveCardsListener(CardInterationListener listener) {
        removeCardsListeners.add(listener);
    }

    public void updateScore(int inc) {
        user.updatePoints(inc);
    }

    abstract void operate();

    class DefaultEnableOperationListener implements EnableOperationListener {

        @Override
        public void doAction(Player player) {
            if (player == Player.this) {
                operate();
            }
        }
    }
}
