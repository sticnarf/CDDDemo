package game;

public class Card {
    private Color color;
    private int figure;

    public Card(Color color, int figure) {
        this.color = color;
        this.figure = figure;
    }

    public Color getColor() {
        return color;
    }

    public int getFigure() {
        return figure;
    }
}
