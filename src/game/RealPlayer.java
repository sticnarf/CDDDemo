package game;

import user.User;

import java.util.List;

public class RealPlayer extends Player {
    private List<Card> selectedCards;

    RealPlayer(Game game, User user) {
        super(game, user);
        // TODO Initial listeners list
    }

    @Override
    void operate() {
        // ?
    }

    @Override
    public void addCards(Card[] cards) {
        super.addCards(cards);
    }

    @Override
    public void removeCards(Card[] cards) {
        super.removeCards(cards);
    }

    public void selectCards(Card[] cards) {

    }

    public void confirm() {
        game.playerDiscard(this, selectedCards.toArray(new Card[]{}));
    }
}
